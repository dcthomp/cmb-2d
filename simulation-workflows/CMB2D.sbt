<?xml version="1.0" encoding="utf-8" ?>
<SMTK_AttributeResource Version="4">

  <!-- Category & Analysis specifications -->
  <Categories>
    <Cat>MinimalFEM</Cat>
    <Cat>GlobalSizingMesher</Cat>
  </Categories>

  <Analyses Exclusive="false">
    <!-- Simulations -->
    <Analysis Type="Simulation" Exclusive="true" Required="true"/>
    <Analysis Type="MinimalFEM" BaseType="Simulation">
      <Cat>MinimalFEM</Cat>
    </Analysis>

    <!-- Meshers -->
    <Analysis Type="Mesher" Exclusive="true" Required="true"/>
    <Analysis Type="XMSMesher" BaseType="Mesher">
      <Cat>GlobalSizingMesher</Cat>
    </Analysis>
  </Analyses>

  <Includes>
    <File>MinimalFEM/MinimalFEM.sbt</File>
    <File>internal/GlobalSizingMesher.sbt</File>
  </Includes>

  <Views>
    <View Type="Group" Title="TopLevel" TopLevel="true" TabPosition="North"
      FilterByAdvanceLevel="true" FilterByCategory="false">
      <Views>
        <View Title="Analysis" />
        <View Title="MinimalFEM" />
        <View Title="GlobalSizingMesher"/>
      </Views>
    </View>

    <View Type="Group" Title="Analysis" Style="Tiled">
      <Views>
        <View Title="SelectModules"/>
      </Views>
    </View>

    <View Type="Analysis" Name="SelectModules" Label="Select Modules"
      AnalysisAttributeName="analysis" AnalysisAttributeType="analysis">
    </View>
  </Views>

</SMTK_AttributeResource>
