<?xml version="1.0" encoding="utf-8" ?>
<SMTK_AttributeResource Version="4">
  <Categories>
    <Cat>GlobalSizingMesher</Cat>
  </Categories>
  <Definitions>
    <AttDef Type="meshing-constraints" Label="Meshing Configuration">
      <Categories>
        <Cat>GlobalSizingMesher</Cat>
      </Categories>
      <ItemDefinitions>
        <Double Name="ratio" Label="Global Sizing Constraint (Relative)">
          <BriefDescription>
            The relative size of a mesh cell w.r.t. the minimum model edge length*.
          </BriefDescription>
          <RangeInfo>
            <Min>0.0</Min>
            <Max>1.0</Max>
          </RangeInfo>
          <DefaultValue>0.1</DefaultValue>
        </Double>
      </ItemDefinitions>
    </AttDef>
  </Definitions>

  <!-- Views -->
  <Views>
    <View Type="Instanced" Name="GlobalSizingMesher" Label="Meshing Configuration">
      <InstancedAttributes>
        <Att Name="MeshingConfiguration" Type="meshing-constraints" />
      </InstancedAttributes>
    </View>
  </Views>
</SMTK_AttributeResource>
