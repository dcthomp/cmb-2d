#include "smtk/simulation/cmb2d/operations/Create.h"
#include "smtk/simulation/cmb2d/Metadata.h"
#include "smtk/simulation/cmb2d/operations/ImportModel.h"
#include "smtk/simulation/cmb2d/utility/AttributeUtils.h"

#include "smtk/simulation/cmb2d/Create_xml.h"

#include "smtk/attribute/DirectoryItem.h"
#include "smtk/attribute/FileItem.h"
#include "smtk/attribute/IntItem.h"
#include "smtk/attribute/Resource.h"
#include "smtk/attribute/ResourceItem.h"
#include "smtk/attribute/StringItem.h"
#include "smtk/attribute/VoidItem.h"
#include "smtk/operation/Manager.h"
#include "smtk/project/Manager.h"

#include <string>

#include <boost/filesystem.hpp>
namespace fs = boost::filesystem;

namespace
{
const int OP_SUCCEEDED = static_cast<int>(smtk::operation::Operation::Outcome::SUCCEEDED);
}

#define LogInternalError(message)                                                                  \
  do                                                                                               \
  {                                                                                                \
    smtkErrorMacro(logger, "Internal Error: " << message);                                         \
    cleanup();                                                                                     \
    return this->createResult(Create::Outcome::FAILED);                                            \
  } while (0)

#define LogError(message)                                                                          \
  do                                                                                               \
  {                                                                                                \
    smtkErrorMacro(logger, "Error: " << message);                                                  \
    cleanup();                                                                                     \
    return this->createResult(Create::Outcome::FAILED);                                            \
  } while (0)

namespace smtk
{
namespace simulation
{
namespace cmb2d
{

Create::Result Create::operateInternal()
{
  auto& logger = this->log();
  std::function<void(void)> cleanup = []() {}; // noop

  Create::Result result = this->createResult(Create::Outcome::SUCCEEDED);

  // Get Workflow location
  fs::path sbtPath;
  if (this->parameters()->findFile("simulation-template")->isEnabled())
  {
    sbtPath = this->parameters()->findFile("simulation-template")->value();
  }
  else
  {
    fs::path workflowRoot(Metadata::WORKFLOW_DIRECTORY);
#ifdef WIN32
    sbtPath = workflowRoot / "CMB2D_Windows.sbt";
#else
    sbtPath = workflowRoot / "CMB2D.sbt";
#endif
  }
  // Make sure the workflow file exists
  if (!fs::exists(sbtPath))
  {
    LogError("Expected sbt file at " << sbtPath.string());
  }

  // Create project directory
  std::string location = this->parameters()->findDirectory("location")->value(0);
  fs::path projectRoot(location);

  bool allowOverwrite = this->parameters()->find("overwrite")->isEnabled();
  if (fs::exists(projectRoot) && !fs::is_empty(projectRoot))
  {
    bool allowOverwrite = this->parameters()->find("overwrite")->isEnabled();
    if (!allowOverwrite)
    {
      LogError(
        "Cannot create project in existing directory (" << location
                                                        << ") unless overwrite flag is set.");
    }
    fs::remove_all(projectRoot);
  }

  if (!fs::exists(projectRoot))
  {
    fs::create_directories(projectRoot);
    // Update the cleanup routine to remove the created directory on failure
    cleanup = [projectRoot]() { fs::remove_all(projectRoot); };
  }

  // Create project
  auto project = this->projectManager()->create(Metadata::PROJECT_TYPENAME);
  if (!project)
  {
    LogInternalError("Could not create project of type \"" << Metadata::PROJECT_TYPENAME << "\"");
  }

  smtk::attribute::ResourceItem::Ptr created = result->findResource("resource");
  bool ok = created->setValue(project);
  if (!ok)
  {
    LogInternalError("Could not assign project to result");
  }

  std::string projectName = projectRoot.filename().string();
  project->setName(projectName);
  fs::path projectFile = projectRoot / (projectName + Metadata::PROJECT_FILE_EXTENSION);
  project->setLocation(projectFile.string());

  project->operations().setManager(m_manager);

  // Import Workflow
  auto attImportOp = this->manager()->create("smtk::attribute::Import");
  if (!attImportOp)
  {
    LogInternalError("Could not create smtk::attribute::Import operation");
  }
  attImportOp->parameters()->findFile("filename")->setValue(sbtPath.string());
  auto opResult = attImportOp->operate();
  int outcome = opResult->findInt("outcome")->value();
  if (outcome != OP_SUCCEEDED)
  {
    LogError("Could not import template file");
  }
  auto res = opResult->findResource("resource")->value();
  smtk::attribute::ResourcePtr attResource =
    std::dynamic_pointer_cast<smtk::attribute::Resource>(res);

  // Add workflow Attribute Resource to the project
  std::string analysisName = this->parameters()->findString("analysis-name")->value();
  attResource->setName(analysisName);
  std::string analysis_role = Metadata::getRoleStr(Metadata::Role::ANALYSIS);
  project->resources().add(attResource, analysis_role);

  // Attempt to import a PPG model
  // It is not an error if this fails
  bool has_model = this->parameters()->find("model-filename")->isEnabled();
  if (has_model)
  {
    auto modelFile = this->parameters()->findFile("model-filename")->value();
    // TODO: Allow reading or importing a model
    auto importModelOp = this->manager()->create("smtk::simulation::cmb2d::ImportModel");
    importModelOp->parameters()->associate(project);
    importModelOp->parameters()->findFile("filename")->setValue(modelFile);
    importModelOp->parameters()->findVoid("overwrite")->setIsEnabled(allowOverwrite);

    auto opResult = importModelOp->operate();
    int outcome = opResult->findInt("outcome")->value();
    if (outcome != OP_SUCCEEDED)
    {
      smtkErrorMacro(logger, "Warning: Could not import " << modelFile);
    }
  }

  return result;
}

const char* Create::xmlDescription() const
{
  return Create_xml;
}

} // namespace cmb2d
} // namespace simulation
} // namespace smtk
