<?xml version="1.0" encoding="utf-8" ?>
<SMTK_AttributeResource Version="4">
  <Definitions>
    <include href="smtk/operation/Operation.xml"/>
    <AttDef Type="cmb2d-run" Label="CMB 2D Project - Run" BaseType="operation">
      <BriefDescription>
        Run a CMB Project. Dispatches to the simulation specific runner.
      </BriefDescription>
      <AssociationsDef Name="project" NumberOfRequiredValues="1" Extensible="false" OnlyResources="true" LockType="DoNotLock">
        <Accepts><Resource Name="smtk::project::Project"/></Accepts>
      </AssociationsDef>

      <ItemDefinitions>
        <Void Name="overwrite" Label="Allow overwrite" Optional="true" IsEnabledByDefault="false"/>
      </ItemDefinitions>
    </AttDef>
    <include href="smtk/operation/Result.xml"/>
    <AttDef Type="result(cmb2d-run)" BaseType="result"/>
  </Definitions>
</SMTK_AttributeResource>
