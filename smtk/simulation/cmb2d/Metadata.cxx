#include "smtk/simulation/cmb2d/Metadata.h"

#include "smtk/project/Project.h"

#include <boost/filesystem.hpp>
namespace fs = boost::filesystem;

namespace smtk
{
namespace simulation
{
namespace cmb2d
{

template <class E, size_t N>
E findEnumFromString(std::string strInput, const std::array<std::string,N>& enumeratedStrings)
{
  std::transform(strInput.begin(), strInput.end(), strInput.begin(), ::toupper);
  int counter = 0;
  for (auto str : enumeratedStrings)
  {
    if (strInput == str)
    {
      break;
    }
    counter++;
  }
  return (E)counter;
}

const std::string Metadata::PROJECT_TYPENAME = "cmb2d";
const std::string Metadata::PROJECT_FILE_EXTENSION = ".project.smtk";
const std::string Metadata::PROPERTY_PREFIX = Metadata::PROJECT_TYPENAME + ".";
const std::array<std::string,Metadata::Role::NROLE> Metadata::ROLES = { "CMB2D-ANALYSIS", "CMB2D-MODEL", "CMB2D-MESH" };
const std::array<std::string, Metadata::Simulation::NSIMULATION> Metadata::SIMULATIONS = {"MinimalFEM"};
const std::array<std::string, Metadata::Mesher::NMESHER> Metadata::MESHERS = {"XMSMesher"};

#ifndef WORKFLOW_SOURCE_DIR
#error "WORKFLOW_SOURCE_DIR is not defined"
#endif
std::string Metadata::WORKFLOW_DIRECTORY = WORKFLOW_SOURCE_DIR;

std::string Metadata::getRoleStr(Role role)
{
  return ROLES[role];
}

std::string Metadata::roleToStr(Role role)
{
  return ROLES[role];
}

Metadata::Role Metadata::strToRole(std::string roleStrInput)
{
  return findEnumFromString<Role>(roleStrInput, ROLES);
}

std::string Metadata::simulationToStr(Simulation sim)
{
  return SIMULATIONS[sim];
}

Metadata::Simulation Metadata::strToSimulation(std::string simStr)
{
  return findEnumFromString<Simulation>(simStr, SIMULATIONS);
}

std::string Metadata::mesherToStr(Mesher mesher)
{
  return MESHERS[mesher];
}

Metadata::Mesher Metadata::strToMesher(std::string mesherStr)
{
  return findEnumFromString<Mesher>(mesherStr, MESHERS);
}

bool Metadata::isUniqueRole(Role role)
{
  return true;
}

bool Metadata::isUniqueRole(std::string roleStr)
{
  return (strToRole(roleStr) != Role::NROLE);
}

void Metadata::addResourceToProject(
  smtk::project::ProjectPtr project,
  smtk::resource::ResourcePtr resource,
  Metadata::Role role)
{
  auto roleStr = Metadata::getRoleStr(role);
  // Remove any resources that is currently in the project with this role
  bool exists = false;
  if (Metadata::isUniqueRole(role))
  {
    auto roleResources = project->resources().findByRole(roleStr);
    fs::path projectRoot = Metadata::getProjectRoot(project);
    for (auto res : roleResources)
    {
      if (resource == res)
      {
        exists = true;
        continue;
      }

      // Remove copied resource source from the <project>/assets directory
      if (res->properties().contains<std::string>("source"))
      {
        fs::path sourcePath = res->properties().at<std::string>("source");
        if (sourcePath.parent_path().parent_path() == projectRoot)
        {
          fs::remove(sourcePath);
        }
        res->properties().erase<std::string>("source");
      }

      fs::path resourcePath = res->location();
      if (resourcePath.parent_path().parent_path() == projectRoot)
      {
        fs::remove(resourcePath);
      }

      // Remove the resource from the project
      project->resources().remove(res);
    }
  }

  if (!exists)
  {
    project->resources().add(resource, roleStr);
  }
}

fs::path Metadata::getAssetPath(smtk::project::ProjectPtr project)
{
  fs::path projectPath = project->location();
  fs::path projectRoot = projectPath.parent_path();
  return projectRoot / "assets";
}

fs::path Metadata::getProjectRoot(smtk::project::ProjectPtr project)
{
  fs::path projectPath = project->location();
  fs::path projectRoot = projectPath.parent_path();
  return projectRoot;
}

} // namespace cmb2d
} // namespace simulation
} // namespace smtk
