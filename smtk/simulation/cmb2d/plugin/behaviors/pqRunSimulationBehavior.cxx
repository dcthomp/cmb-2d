#include "pqRunSimulationBehavior.h"

#include "smtk/simulation/cmb2d/operations/Run.h"
#include "smtk/simulation/cmb2d/plugin/pqSelectDirectoryDialog.h"
#include "smtk/simulation/cmb2d/qt/qtProjectRuntime.h"

// SMTK
#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/VoidItem.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKBehavior.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKWrapper.h"
#include "smtk/extension/qt/qtOperationDialog.h"
#include "smtk/io/Logger.h"
#include "smtk/operation/Manager.h"
#include "smtk/operation/Operation.h"
#include "smtk/project/Project.h"
#include "smtk/project/operators/Write.h"

// ParaView
#include "pqActiveObjects.h"
#include "pqCoreUtilities.h"
#include "pqServer.h"

namespace smtk
{
namespace simulation
{
namespace cmb2d
{

// Reaction

pqRunSimulationReaction::pqRunSimulationReaction(QAction* parent)
  : Superclass(parent)
{
}

pqRunSimulationReaction::~pqRunSimulationReaction() {}

void pqRunSimulationReaction::onTriggered()
{
  pqRunSimulationBehavior::instance()->runSimulation();
}

// Behavior

pqRunSimulationBehavior::pqRunSimulationBehavior(QObject* parent)
  : Superclass(parent)
{
}

pqRunSimulationBehavior::~pqRunSimulationBehavior() {}

namespace
{
const int OP_SUCCEEDED = static_cast<int>(smtk::operation::Operation::Outcome::SUCCEEDED);
}

void pqRunSimulationBehavior::runSimulation()
{
  auto project = qtProjectRuntime::instance()->project();
  if (!project)
  {
    return;
  }

  // Get the SMTK wrapper from paraview
  pqServer* server = pqActiveObjects::instance().activeServer();
  pqSMTKWrapper* wrapper = pqSMTKBehavior::instance()->resourceManagerForServer(server);
  auto opManager = wrapper->smtkOperationManager();

  // Create the export operation
  auto runSimOp = opManager->create<smtk::simulation::cmb2d::Run>();
  assert(runSimOp);
  runSimOp->parameters()->associate(project);

  runSimOp->parameters()->findVoid("overwrite")->setIsEnabled(true);
  runSimOp->operate();

  return;
}

} // namespace cmb2d
} // namespace simulation
} // namespace smtk
