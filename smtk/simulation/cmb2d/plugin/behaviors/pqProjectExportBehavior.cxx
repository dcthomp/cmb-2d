#include "smtk/simulation/cmb2d/plugin/behaviors/pqProjectExportBehavior.h"

#include "smtk/simulation/cmb2d/operations/Export.h"
#include "smtk/simulation/cmb2d/plugin/pqSelectDirectoryDialog.h"

#include "smtk/simulation/cmb2d/qt/qtProjectRuntime.h"

// SMTK
#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/DirectoryItem.h"
#include "smtk/attribute/FileItem.h"
#include "smtk/attribute/IntItem.h"
#include "smtk/attribute/ResourceItem.h"
#include "smtk/attribute/VoidItem.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKBehavior.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKWrapper.h"
#include "smtk/extension/qt/qtOperationDialog.h"
#include "smtk/model/Resource.h"
#include "smtk/operation/Manager.h"
#include "smtk/operation/Operation.h"
#include "smtk/project/Project.h"

// ParaView
#include "pqActiveObjects.h"
#include "pqCoreUtilities.h"
#include "pqServer.h"

// Qt
#include <QAction>
#include <QObject>
#include <QSharedPointer>

#include <memory>

namespace
{
int OP_SUCCEEDED = static_cast<int>(smtk::operation::Operation::Outcome::SUCCEEDED);
}

namespace smtk
{
namespace simulation
{
namespace cmb2d
{
pqProjectExportReaction::pqProjectExportReaction(QAction* parent)
  : Superclass(parent)
{
}

void pqProjectExportReaction::onTriggered()
{
  pqProjectExportBehavior::instance()->exportSimulation();
}

//------------------------------------------
pqProjectExportBehavior::pqProjectExportBehavior(QObject* parent)
  : Superclass(parent)
{
}

pqProjectExportBehavior::~pqProjectExportBehavior() {}

void pqProjectExportBehavior::exportSimulation()
{
  // The the active project
  auto project = qtProjectRuntime::instance()->project();
  if (!project)
  {
    // TODO: Log an error
    return;
  }

  // Get the SMTK wrapper from paraview
  pqServer* server = pqActiveObjects::instance().activeServer();
  pqSMTKWrapper* wrapper = pqSMTKBehavior::instance()->resourceManagerForServer(server);
  auto opManager = wrapper->smtkOperationManager();

  // Create the export operation
  auto exportOp = opManager->create<smtk::simulation::cmb2d::Export>();
  exportOp->parameters()->associate(project);
  assert(exportOp);
  auto result = exportOp->operate();
  int outcome = result->findInt("outcome")->value();
  if (outcome != OP_SUCCEEDED)
  {
    // Todo popup error message
    return;
  }

  Q_EMIT this->simulationExported();
}

} // namespace cmb2d
} // namespace simulation
} // namespace smtk
