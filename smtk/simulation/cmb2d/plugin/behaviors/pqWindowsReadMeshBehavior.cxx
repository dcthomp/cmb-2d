//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "pqWindowsReadMeshBehavior.h"

// CMB2D Extension includes
#include "smtk/simulation/cmb2d/Metadata.h"
#include "smtk/simulation/cmb2d/qt/qtProjectRuntime.h"

// SMTK includes
#include "smtk/attribute/Definition.h"
#include "smtk/attribute/FileItem.h"
#include "smtk/attribute/FileItemDefinition.h"
#include "smtk/attribute/IntItem.h"
#include "smtk/attribute/ResourceItem.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKBehavior.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKWrapper.h"
#include "smtk/mesh/operators/ReadResource.h"
#include "smtk/operation/Manager.h"
#include "smtk/project/Manager.h"

// ParaView includes
#include "pqActiveObjects.h"
#include "pqCoreUtilities.h"
#include "pqFileDialog.h"

// Qt includes
#include <QAction>
#include <QDebug>
#include <QMessageBox>

namespace smtk
{
namespace simulation
{
namespace cmb2d
{

namespace
{
const int OP_SUCCESS = static_cast<int>(smtk::operation::Operation::Outcome::SUCCEEDED);
} // namespace

//-----------------------------------------------------------------------------
pqWindowsReadMeshReaction::pqWindowsReadMeshReaction(QAction* parentObject)
  : Superclass(parentObject)
{
}

void pqWindowsReadMeshReaction::onTriggered()
{
  pqWindowsReadMeshBehavior::instance()->readResource();
}

//-----------------------------------------------------------------------------
static pqWindowsReadMeshBehavior* g_instance = nullptr;

pqWindowsReadMeshBehavior::pqWindowsReadMeshBehavior(QObject* parent)
  : Superclass(parent)
{
}

pqWindowsReadMeshBehavior* pqWindowsReadMeshBehavior::instance(QObject* parent)
{
  if (!g_instance)
  {
    g_instance = new pqWindowsReadMeshBehavior(parent);
  }

  if (g_instance->parent() == nullptr && parent)
  {
    g_instance->setParent(parent);
  }

  return g_instance;
}

pqWindowsReadMeshBehavior::~pqWindowsReadMeshBehavior()
{
  if (g_instance == this)
  {
    g_instance = nullptr;
  }

  QObject::disconnect(this);
}

void pqWindowsReadMeshBehavior::readResource()
{
  auto project = smtk::simulation::cmb2d::qtProjectRuntime::instance()->project();
  if (project == nullptr)
  {
    qWarning() << "Cannot read mesh because no project currently loaded.";
    return;
  }

  pqServer* server = pqActiveObjects::instance().activeServer();
  pqSMTKWrapper* wrapper = pqSMTKBehavior::instance()->resourceManagerForServer(server);
  auto opManager = wrapper->smtkOperationManager();

  // Read the mesh
  auto readOp = opManager->create<smtk::mesh::ReadResource>();

  // Open a file dialog
  QString location;
  {
    // Get the file filters from the operation
    smtk::attribute::DefinitionPtr def = readOp->parameters()->definition();
    int itemPosition = def->findItemPosition("filename");
    smtk::attribute::ItemDefinitionPtr itemDef = def->itemDefinition(itemPosition);
    auto fileItemDef = std::dynamic_pointer_cast<smtk::attribute::FileItemDefinition>(itemDef);
    std::string filters = fileItemDef->getFileFilters();

    // Construct a file dialog for the user to select the mesh file
    QString filter(filters.c_str());
    pqFileDialog fileDialog(
      server, pqCoreUtilities::mainWidget(), tr("Select Mesh File:"), QString(), filter);
    fileDialog.setObjectName("FileOpenDialog");
    fileDialog.setFileMode(pqFileDialog::ExistingFile);
    fileDialog.setShowHidden(true);
    if (fileDialog.exec() != QDialog::Accepted)
    {
      return;
    }
    location = fileDialog.getSelectedFiles()[0];
  }

  readOp->parameters()->associate(project);
  readOp->parameters()->findFile("filename")->setValue(location.toStdString());
  auto result = readOp->operate();
  int outcome = result->findInt("outcome")->value();
  if (outcome != OP_SUCCESS)
  {
    qWarning() << readOp->log().convertToString().c_str();
    QMessageBox::warning(
      pqCoreUtilities::mainWidget(),
      "Error",
      "Unable to read mesh file; see \"Output Messages\" for more info.");
    return;
  }

  // add the mesh to the project
  smtk::resource::Resource::Ptr meshResource = result->findResource("resource")->value();
  auto meshRoleStr = Metadata::roleToStr(Metadata::Role::MESH);
  project->resources().add(meshResource, meshRoleStr);
  meshResource->setClean(false);
  project->setClean(false);

  Q_EMIT this->meshRead();
} // readResource()

} // namespace cmb2d
} // namespace simulation
} // namespace smtk
