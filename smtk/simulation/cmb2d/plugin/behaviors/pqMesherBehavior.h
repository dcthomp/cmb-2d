#ifndef smtk_simulation_cmb2d_plugin_behaviors_pqMesherBehavior_h
#define smtk_simulation_cmb2d_plugin_behaviors_pqMesherBehavior_h

#include "pqReaction.h"

#include <QObject>

namespace smtk
{
namespace simulation
{
namespace cmb2d
{

class pqMesherReaction : public pqReaction
{
  Q_OBJECT
  typedef pqReaction Superclass;

public:
  /**
  * Constructor. Parent cannot be NULL.
  */
  pqMesherReaction(QAction* parent);

protected:
  /**
  * Called when the action is triggered.
  */
  void onTriggered() override;

private:
  Q_DISABLE_COPY(pqMesherReaction)
};

class pqMesherBehavior : public QObject
{
  Q_OBJECT
  using Superclass = QObject;

public:
  static pqMesherBehavior* instance(QObject* parent = nullptr);
  ~pqMesherBehavior() override;

  void generateMesh();

Q_SIGNALS:
  void meshGenerated();

protected:
  pqMesherBehavior(QObject* parent);

private:
  Q_DISABLE_COPY(pqMesherBehavior);
};

} // namespace cmb2d
} // namespace simulation
} // namespace smtk

#endif
