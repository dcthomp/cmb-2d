#ifndef smtk_simulation_cmb2d_plugin_behaviors_pqRunBehavior_h
#define smtk_simulation_cmb2d_plugin_behaviors_pqRunBehavior_h

#include "smtk/simulation/cmb2d/qt/qtInstanced.h"

#include "pqReaction.h"

namespace smtk
{
namespace simulation
{
namespace cmb2d
{

class pqRunSimulationReaction : public pqReaction
{
  Q_OBJECT
  using Superclass = pqReaction;

public:
  pqRunSimulationReaction(QAction* parent);
  ~pqRunSimulationReaction() override;

  void onTriggered() override;

private:
  Q_DISABLE_COPY(pqRunSimulationReaction)
};


class pqRunSimulationBehavior : public qtInstanced<pqRunSimulationBehavior>
{
  Q_OBJECT
  using Superclass = qtInstanced<pqRunSimulationBehavior>;
  friend Superclass;
public:
  ~pqRunSimulationBehavior() override;

  void runSimulation();

protected:
  pqRunSimulationBehavior(QObject* parent = nullptr);
};

} // namespace cmb2d
} // namespace simulation
} // namespace smtk

#endif // smtk_simulation_cmb2d_plugin_behaviors_pqRunBehavior_h
