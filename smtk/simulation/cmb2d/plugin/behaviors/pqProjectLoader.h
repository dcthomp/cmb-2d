//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_simulation_cmb2d_plugin_behaviors_pqProjectLoader_h
#define smtk_simulation_cmb2d_plugin_behaviors_pqProjectLoader_h

#include "smtk/PublicPointerDefs.h"

#include <QObject>

class pqServer;
class pqServerResource;

namespace smtk
{
namespace simulation
{
namespace cmb2d
{

/** \brief Handles opening projects from main menu and recent projects list
  */
class pqProjectLoader : public QObject
{
  Q_OBJECT
  typedef QObject Superclass;

public:
  static pqProjectLoader* instance(QObject* parent = nullptr);
  ~pqProjectLoader() override;

  // Indicates if paraview resource can be loaded
  bool canLoad(const pqServerResource& resource) const;

  // Open project from path on server
  bool load(pqServer* server, const QString& path);

  // Open project from recent projects list
  bool load(const pqServerResource& resource);

Q_SIGNALS:
  void projectOpened(smtk::project::ProjectPtr);

protected:
  pqProjectLoader(QObject* parent = nullptr);

private:
  Q_DISABLE_COPY(pqProjectLoader);
};

} // namespace cmb2d
} // namespace simulation
} // namespace smtk

#endif
