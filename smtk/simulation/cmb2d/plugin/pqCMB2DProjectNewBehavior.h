//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_simulation_cmb2d_plugin_pqCMB2DProjectNewBehavior_h
#define smtk_simulation_cmb2d_plugin_pqCMB2DProjectNewBehavior_h

#include "smtk/PublicPointerDefs.h"

#include "pqReaction.h"

#include <QObject>

/// A reaction for writing a resource manager's state to disk.
class pqCMB2DProjectNewReaction : public pqReaction
{
  Q_OBJECT
  typedef pqReaction Superclass;

public:
  /// Constructor. Parent cannot be NULL.
  pqCMB2DProjectNewReaction(QAction* parent);
  ~pqCMB2DProjectNewReaction() = default;

protected:
  /// Called when the action is triggered.
  void onTriggered() override;

private:
  Q_DISABLE_COPY(pqCMB2DProjectNewReaction)
};

/// \brief Add a menu item for writing the state of the resource manager.
class pqCMB2DProjectNewBehavior : public QObject
{
  Q_OBJECT
  using Superclass = QObject;

public:
  static pqCMB2DProjectNewBehavior* instance(QObject* parent = nullptr);
  ~pqCMB2DProjectNewBehavior() override;

  void newProject();

Q_SIGNALS:
  void projectCreated(smtk::project::ProjectPtr);

protected:
  pqCMB2DProjectNewBehavior(QObject* parent = nullptr);

private:
  class Internal;
  Internal* m_internal;

  Q_DISABLE_COPY(pqCMB2DProjectNewBehavior);
};

#endif
