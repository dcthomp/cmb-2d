#ifndef smtk_simulation_cmb2d_plugin_pqSelecteDirectory_h
#define smtk_simulation_cmb2d_plugin_pqSelecteDirectory_h

#include <string>

#include <QObject>

class pqServer;

namespace smtk
{
namespace simulation
{
namespace cmb2d
{

/** Wrapper around the pqFileDialog for selecting a directory
 */
class pqSelectDirectoryDialog : public QObject
{
  Q_OBJECT
  using Superclass = QObject;

public:
  pqSelectDirectoryDialog(QObject* parent, pqServer* server = nullptr);

  bool exec();

  std::string directory();

  // TODO: Set title
  // void setTitle(const std::string&);
  // TODO: Toggle overwrite
  // void allowOverwrite(bool allow_overwrite = true);

private:
  Q_DISABLE_COPY(pqSelectDirectoryDialog);

  struct Internals;
  Internals *m_internals;
};

} // namespace cmb2d
} // namespace simulation
} // namespace smtk

#endif // smtk_simulation_cmb2d_plugin_pqSelecteDirectory_h
