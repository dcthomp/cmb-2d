#include "smtk/simulation/cmb2d/plugin/pqCMB2DAutoStart.h"
#include "smtk/simulation/cmb2d/Registrar.h"
#include "smtk/simulation/cmb2d/qt/qtProjectRuntime.h"

// SMTK
#include "smtk/extension/paraview/appcomponents/pqSMTKBehavior.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKWrapper.h"
#include "smtk/extension/paraview/server/vtkSMTKSettings.h"

// ParaView
#include "pqActiveObjects.h"
#include "pqApplicationCore.h"
#include "pqAutoApplyReaction.h"
#include "pqCoreUtilities.h"
#include "pqObjectBuilder.h"
#include "pqRenderView.h"
#include "pqServer.h"
#include "vtkPVRenderView.h"
#include "vtkSMIntVectorProperty.h"
#include "vtkSMProperty.h"
#include "vtkSMPropertyHelper.h"
#include "vtkSMProxy.h"
#include "vtkSMSessionProxyManager.h"

pqCMB2DAutoStart::pqCMB2DAutoStart(QObject* parent)
  : Superclass(parent)
{
}

pqCMB2DAutoStart::~pqCMB2DAutoStart() {}

void pqCMB2DAutoStart::startup()
{
  smtk::simulation::cmb2d::qtProjectRuntime::instance(pqCoreUtilities::mainWidget());

  // Check for current/active pqServer
  pqServer* server = pqActiveObjects::instance().activeServer();
  if (server != nullptr)
  {
    pqSMTKWrapper* wrapper = pqSMTKBehavior::instance()->resourceManagerForServer(server);
    this->resourceManagerAdded(wrapper, server);
  }

  // Listen for server connections
  auto smtkBehavior = pqSMTKBehavior::instance();
  QObject::connect(
    smtkBehavior,
    static_cast<void (pqSMTKBehavior::*)(pqSMTKWrapper*, pqServer*)>(
      &pqSMTKBehavior::addedManagerOnServer),
    this,
    &pqCMB2DAutoStart::resourceManagerAdded);

  // Force the auto-apply option to be on
  pqAutoApplyReaction::setAutoApply(true);

  // Force renderviews to 2D mode
  pqObjectBuilder* objectBuilder = pqApplicationCore::instance()->getObjectBuilder();
  QObject::connect(objectBuilder, &pqObjectBuilder::viewCreated, [](pqView* view) {
    pqRenderView* renderView = dynamic_cast<pqRenderView*>(view);
    if (renderView != nullptr)
    {
      vtkSMPropertyHelper(renderView->getProxy(), "InteractionMode")
        .Set(vtkPVRenderView::INTERACTION_MODE_2D);
      renderView->getProxy()->UpdateProperty("InteractionMode", 0);
    }
  });
}

void pqCMB2DAutoStart::shutdown() {}

void pqCMB2DAutoStart::resourceManagerAdded(pqSMTKWrapper* wrapper, pqServer* server)
{
  if (!wrapper || !server)
  {
    return;
  }

  smtk::resource::ManagerPtr resManager = wrapper->smtkResourceManager();
  smtk::view::ManagerPtr viewManager = wrapper->smtkViewManager();
  smtk::operation::ManagerPtr opManager = wrapper->smtkOperationManager();
  smtk::project::ManagerPtr projManager = wrapper->smtkProjectManager();
  if (!resManager || !viewManager || !opManager || !projManager)
  {
    return;
  }

  // Update some default settings
  vtkSMProxy* proxy = server->proxyManager()->GetProxy("settings", "SMTKSettings");
  if (proxy)
  {
    // Disable the smtk save-on-close dialog (superseded in our close behavior)
    vtkSMProperty* ssProp = proxy->GetProperty("ShowSaveResourceOnClose");
    vtkSMIntVectorProperty* ssIntProp = vtkSMIntVectorProperty::SafeDownCast(ssProp);
    if (ssIntProp != nullptr)
    {
      ssIntProp->SetElement(0, vtkSMTKSettings::DontShowAndDiscard);
    } // if (ssIntProp)

    // Enable highlight on hover
    vtkSMProperty* hhProp = proxy->GetProperty("HighlightOnHover");
    vtkSMIntVectorProperty* hhIntProp = vtkSMIntVectorProperty::SafeDownCast(hhProp);
    if (hhIntProp != nullptr)
    {
      hhIntProp->SetElement(0, 1);
    } // if (ssIntProp)

    proxy->UpdateVTKObjects();
  } // if (proxy)
}
