#include "smtk/simulation/cmb2d/Exports.h"

#include "smtk/project/Manager.h"

namespace smtk
{
namespace simulation
{
namespace cmb2d
{

class SMTKCMB2D_EXPORT Registrar
{
public:
  static void registerTo(const smtk::project::Manager::Ptr&);
  static void unregisterFrom(const smtk::project::Manager::Ptr&);
};

} // namespace cmb2d
} // namespace simulation
} // namespace smtk
