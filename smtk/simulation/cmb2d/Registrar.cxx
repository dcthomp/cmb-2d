#include "smtk/simulation/cmb2d/Registrar.h"

#include "smtk/simulation/cmb2d/operations/Add.h"
#include "smtk/simulation/cmb2d/operations/Create.h"
#include "smtk/simulation/cmb2d/operations/Export.h"
#include "smtk/simulation/cmb2d/operations/ImportModel.h"
#include "smtk/simulation/cmb2d/operations/Run.h"

#include "smtk/simulation/cmb2d/Metadata.h"

#include <tuple>

namespace smtk
{
namespace simulation
{
namespace cmb2d
{

namespace
{
using OperationList = std::tuple<Add, Create, Export, ImportModel, Run>;
}

void Registrar::registerTo(const smtk::project::Manager::Ptr& projectManager)
{
  projectManager->registerProject(Metadata::PROJECT_TYPENAME);
  projectManager->registerOperations<OperationList>();
}

void Registrar::unregisterFrom(const smtk::project::Manager::Ptr& projectManager)
{
  projectManager->unregisterProject(Metadata::PROJECT_TYPENAME);
  projectManager->unregisterOperations<OperationList>();
}

} // namespace cmb2d
} // namespace simulation
} // namespace smtk
