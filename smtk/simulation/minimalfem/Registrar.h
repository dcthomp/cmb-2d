#ifndef smtk_simulation_minimalfem_Registrar_h
#define smtk_simulation_minimalfem_Registrar_h

#include "smtk/simulation/minimalfem/Exports.h"

#include "smtk/PublicPointerDefs.h"

namespace smtk
{
namespace simulation
{
namespace minimalfem
{

class SMTKMINIMALFEM_EXPORT Registrar
{
public:
  static void registerTo(const smtk::operation::ManagerPtr&);
  static void unregisterFrom(const smtk::operation::ManagerPtr&);
};

} // namespace minimalfem
} // namespace simulation
} // namespace smtk

#endif // smtk_simulation_minimalfem_Registrar_h
