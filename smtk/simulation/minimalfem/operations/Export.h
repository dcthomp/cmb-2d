#ifndef smtk_simulation_minimalfem_operations_Export_h
#define smtk_simulation_minimalfem_operations_Export_h

#include "smtk/simulation/minimalfem/Exports.h"

#include "smtk/operation/XMLOperation.h"

namespace smtk
{
namespace simulation
{
namespace minimalfem
{

class Export : public smtk::operation::XMLOperation
{
public:
  smtkTypeMacro(smtk::simulation::minimalfem::Export);
  smtkCreateMacro(Export);
  smtkSharedFromThisMacro(smtk::operation::Operation);
  smtkSuperclassMacro(smtk::operation::XMLOperation);

protected:
  Result operateInternal() override;
  const char* xmlDescription() const override;
};

} // namespace minimalfem
} // namespace simulation
} // namespace smtk

#endif // smtk_simulation_minimalfem_operations_Export_h
