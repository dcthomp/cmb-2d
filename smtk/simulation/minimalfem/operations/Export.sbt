<?xml version="1.0" encoding="utf-8" ?>
<SMTK_AttributeResource Version="4">
  <Definitions>
    <include href="smtk/operation/Operation.xml"/>
    <AttDef Type="minimalfem-export" Label="CMB 2D Project - Create" BaseType="operation">
      <BriefDescription>
        Export a MinimalFEM project to simulation inputs
      </BriefDescription>

      <ItemDefinitions>
        <Directory Name="workflow-directory" Label="Workflow Directory" NumberOfRequiredValues="1">
          <BriefDescription>The folder in the local filesystem containing the MinimalFEM workflows.</BriefDescription>
        </Directory>
        <File Name="filename" Label="Input Filename"
              Optional="true"
              FileFilters="Input files (*.inp)">
          <BriefDescription>File to export inputs to</BriefDescription>
        </File>
        <Resource Name="analysis" HoldReference="true" LockType="DoNotLock">
          <Accepts>
            <Resource Name="smtk::attribute::Resource"/>
          </Accepts>
        </Resource>
        <Resource Name="mesh" HoldReference="true" LockType="DoNotLock">
          <Accepts>
            <Resource Name="smtk::mesh::Resource"/>
          </Accepts>
        </Resource>
        <Void Name="overwrite" Label="Allow overwrite" Optional="true" IsEnabledByDefault="false"/>
      </ItemDefinitions>
    </AttDef>
    <include href="smtk/operation/Result.xml"/>
    <AttDef Type="result(minimalfem-export)" BaseType="result"/>
  </Definitions>
</SMTK_AttributeResource>
