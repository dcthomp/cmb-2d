<?xml version="1.0" encoding="utf-8" ?>
<SMTK_AttributeResource Version="4">
  <Definitions>
    <include href="smtk/operation/Operation.xml"/>
    <AttDef Type="minimalfem-run" Label="CMB 2D Project - Create" BaseType="operation">
      <BriefDescription>
        Run a MinimalFEM simulation
      </BriefDescription>

      <ItemDefinitions>
        <Directory Name="run-directory" Label="Run Directory">
          <BriefDescription>The directory to run the simulation</BriefDescription>
        </Directory>

        <String Name="inp-filename" Label="Input Filename" Optional="true" AdvanceLevel="1">w
          <DefaultValue>minfem.inp</DefaultValue>
        </String>
        <String Name="out-filename" Label="Output Filename" Optional="true" AdvanceLevel="1">
          <DefaultValue>minfem.vtp</DefaultValue>
        </String>
        <Int Name="out-format" Label="Format" Optional="true" AdvanceLevel="1">
          <DiscreteInfo DefaultIndex="1">
            <Value Enum="MinimalFEM Output (.out)">0</Value>
            <Value Enum="VTK Polygon Data (.vtp)">1</Value>
          </DiscreteInfo>
        </Int>

        <Void Name="overwrite" Label="Allow overwrite" Optional="true" IsEnabledByDefault="true"/>
      </ItemDefinitions>
    </AttDef>
    <include href="smtk/operation/Result.xml"/>
    <AttDef Type="result(minimalfem-run)" BaseType="result"/>
  </Definitions>
</SMTK_AttributeResource>
