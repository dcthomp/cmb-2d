Viewing the Results
===================

Because :program:`ModelBuilder-2D` incorporates the full :program:`ParaView` visualization feature set, there are numerous ways to display the stress and displacement data computed by :program:`MinimalFEM` After enabling the :program:`ParaView` visualization features and loading the computed data, we will show three different ways to display the data.


.. rst-class:: step-number

18\. Enable ParaView Features

By default, :program:`ModelBuilder-2D` hides the :program:`ParaView` pipeline inspector and related toolbars and filters. To see them, find and click the toolbar button with a gray ParaView icon. Once clicked, the button will change to display the familiar red-green-blue ParaView logo, a number of new toolbars will be displayed, and the :guilabel:`Pipeline Browser` view will display in the sidebar.

.. image:: ./images/toolbutton-paraview.png

For convenience, undock the :guilabel:`Pipeline Browser` and drag it over the :guilabel:`Attribute Editor` view so that it is displayed as another tab.


.. rst-class:: step-number

19\. Add a Third Layout and RenderView

Follow the same steps as before (:ref:`Add a RenderView <add-renderview>`) to create a (third) RenderView tab:

* In the tab bar above the current RenderView, click the :guilabel:`+` button.
* In the new tab, click the :guilabel:`Render View` button.
* Click the :guilabel:`Camera Reset` button to center the model and mesh contents.
* Right click on the content and select :guilabel:`Hide`. You will need to do this twice to hide both the model and mesh respectively.
* Right click on the new tab label and rename it "Results".


.. rst-class:: step-number

20\. Load the VTP Data

In the previous step (:ref:`Running the MinimalFEM Solver <run-minifem>`) the results of the elasticity calculations were written to a ``minfem.vtp`` file under the project directory. Go to the :guilabel:`File` menu and select the :guilabel:`Open...` item, navigate to your project directory, then the ``export`` subdirectory, select the ``minfem.vtp`` file, and click the :guilabel:`OK` button. ModelBuilder will display a dialog titled :guilabel:`Open Data With...` listing a number of readers for VTK polydata files. Choose the ``XML PolyData Reader`` and click the :guilabel:`OK` button. ModelBuilder-2D will load the file and render a basic 2-D display. That display will show the computed stress values; because they are calculated on mesh elements, you can see faceting in the data.

To produce a smooth display, go to the :guilabel:`Filters` menu, open the :guilabel:`Alphabetical` submenu and select the :guilabel:`Cell Data To Point Data` item. This creates a new set of point data that can be interpolated by the graphics hardware.

.. image:: ./images/results-loaded.png


.. rst-class:: step-number

21\. Open the Properties View

The main visualization features can be accessed from the :guilabel:`Properties` view. To open this view, go to the :guilabel:`View` menu and check the box next to :guilabel:`Properties`. When the :guilabel:`Properties` view appears in the sidebar, you can undock it and drag it over the other tabbed views and dock it as another tab.


.. rst-class:: step-number

22\. Add WarpByVector Filter

To visualize physical distortion, first go back to the original display settings - do this by going to the :guilabel:`Pipeline Browser` tab and use the eyeball icons to hide ```CellDataToPointData1`` and show ``minfem.vtp``. Then select the ``minfem.vtp`` item, go to the :guilabel:`Filters` menu, select the :guilabel:`Common` submenu, and select :guilabel:`Warp By Vector`.

In the :guilabel:`Properties` tab, you will find three top-level sections labeld :guilabel:`Properties (WarnByVector1)`, :guilabel:`Display (GeometryRepresentation)`, and :guilabel:`View (Render View)`. We suggest that you collapse all three sections and open them one at a time to make these changes.

* In the first section -- :guilabel:`Properties (WarnByVector1)`` -- set the :guilabel:`Scale Factor` to ``25``.
* In the second section -- :guilabel:`Display (GeometryRepresentation)` -- set the :guilabel:`Coloring` to ``displacement``.

.. note:: After typing a numerical field in the :guilabel:`Properties View`, either hit the :kbd:`<Enter>` key or click the :guilabel:`Apply` button to apply the change.

With these settings, you can see bowing of the upper edge and skewing of the hole geometry.

.. image:: ./images/results-warp.png


.. rst-class:: step-number

23\. Add Glyph Filter

To display the strain field with vector arrows, go to the :guilabel:`Pipeline Browser` tab and:

* hide the ``WarpByVector1`` item,
* show the ``CellDataToPointData1`` item,
* select the ``minfem.vtp`` item.

Then go to the :guilabel:`Filters` menu, select the :guilabel:`Common` submenu, and select ``Glyph``. You will see some warning messages in the :guilabel:`Output Messages` view, which we will deal with next. (You can clear the Output Messages.)

The :guilabel:`Properties` view now has three top-level sections labeled :guilabel:`Properties (Glyph1)`, :guilabel:`Display (GeometryRepresentation)`, and :guilabel:`View (Render View)`. We suggest you collapse all three before setting the following properties:

Expand the :guilabel:`Properties (Glyph1)` section and

* Find the :guilabel:`Scale` subsection and set :guilabel:`Scale Array` to ``displacement``.
* In the :guilabel:`Scale` subsection, set :guilabel:`Scale Factor` to ``50``.
* Find the :guilabel:`Masking` subsection and set the :guilabel:`Maximum Number Of Sample Points` to ``400``.
* Collapse the :guilabel:`Properties (Glyph1)` section.

Expand the :guilabel:`Display (GeometryRepresentation)` section and:

* Find the :guilabel:`Coloring` subsection and set it to ``displacement``.

When you are done, you should see a set of arrow-shaped glyphs colored and scaled by the magnitude of computed displacement data, with the polygon colored by the computed stress data.

.. image:: ./images/results-glyph.png

These examples should give you some idea of what you can do with ModelBuilder and the CMB platform. With the full range of :guilabel:`ParaView`` feature available, you can explore many more properties, filters, and other options.
