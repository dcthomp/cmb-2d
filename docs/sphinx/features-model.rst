Importing Geometry
==================

Our next step is to construct a geometric model to represent the shape and topology of our problem domain. ModelBuilder can be used to create geometric models interactively, or import geometry from a variety of file formats including Open Cascade, Genesis, and STL.

For two-dimensional problems, we created a simple *Planar PolyGon* (PPG) file format that you can use to specify two-dimensional geometry with a text editor. This format defines geometry as a list of vertex coordinates and polygon faces, with vertices connected implicitly by straight-line model edges. The geometry for our example problem can be defined in PPG format as follows:

.. include:: ./_downloads/example.ppg
   :literal:

Because the PPG import software is intended for training and demonstration use, the supported feature set is purposely limited. Each polygon must have simple geomety, polygons cannot be self-intersecting, polygons may share edges but cannot otherwise intersect or overlap. More details can be found at this `PPG File Format <https://smtk.readthedocs.io/en/latest/userguide/model/session-polygon.html#ppg-file-format>`_ page.


.. rst-class:: step-number

3\. Download PPG File

To add this geometry to the project, you can download the ``example.ppg`` file from the following link (right click on the link and find the "Save As" option), or copy the contents shown above into a file.

:download:`example.ppg <./_downloads/example.ppg>`


.. rst-class:: step-number

4\. Import Geometry

Go to the "CMB2D" menu and select the :guilabel:`Import Geometry...` item, which displays a file dialog for entering the ppg file. Navigate to the ppg file and click the :guilabel:`OK` button. In response, :program:`ModelBuilder-2D` will read the ppg file, generate a *model resource*, add it to the project resources, and display the model in the 3-D RenderView. Find and click the :guilabel:`Camera Reset` toolbar button to center the data in the display.

.. image:: ./images/toolbutton-camera-reset.png

When you are done, modelbuilder should look similar to this

.. image:: ./images/model-imported.png

With the geometry model added to the project, we next begin the analysis specification.
